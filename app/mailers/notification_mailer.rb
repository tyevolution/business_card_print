# frozen_string_literal: true

class NotificationMailer < ApplicationMailer
  def send_auth_code(user, auth_code)
    @user = user
    @auth_code = auth_code
    mail(
      subject: '[マイ名刺]認証コード通知',
      to: @user.email, &:text
    )
  end

  def send_inquiry(params)
    @email = params[:email]
    @inquiry_type = case params[:inquiry_type].to_i
                    when Inquiry::BUG
                      Inquiry::BUG_STRING
                    when Inquiry::REQUEST
                      Inquiry::REQUEST_STRING
                    when Inquiry::OTHER
                      Inquiry::OTHER_STRING
                    else
                      ''
                    end
    @content = params[:content]
    mail(
      subject: '[マイ名刺]お問い合わせがありました',
      to: ENV['INQUIRY_MAIL_ADDRESS'], &:text
    )
  rescue StandardError => e
    LoggerFormatUtil.error_log(e, 'send_inquiry')
    nil
  end

  def send_delete_user(user_email)
    @user_email = user_email
    mail(
      subject: '[マイ名刺]アカウントを削除しました',
      to: @user_email, &:text
    )
  end
end
