# frozen_string_literal: true

module ApplicationHelper
  def body_class
    @body_class || ''
  end

  def conversion_expiration_date(expiration_date)
    expiration_date.to_s.in_time_zone.strftime('%Y/%m/%d %H:%M')
  end

  def nl_to_br(str)
    h(str).gsub(/\R/, '<br>')
  end

  def iOS_link
    'https://apps.apple.com/us/app/my名刺/id1514377880?l=ja&ls=1'
  end

  def android_link
    'https://play.google.com/store/apps/details?id=com.mymeishi'
  end
end
