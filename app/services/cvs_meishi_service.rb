# frozen_string_literal: true

class CvsMeishiService
  require 'net/http/post/multipart'

  def send(file_name, size, cvtype = 0, user_id = nil)
    params = {
      printsize: size.to_s,
      cvtype: cvtype.to_i
    }

    url_str = ENV['API_URL'].to_s
    uri = URI.parse(url_str)

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true if url_str.include?('https://')
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    http.read_timeout = 180

    response_data = nil

    if Rails.env.production?
      File.open("#{UploadImage::DIR_PATH}/#{file_name}") do |file|
        params[:filedata] = UploadIO.new(file, 'image/jpeg', "#{file_name}.jpeg")
        params[:checksum] = Digest::SHA1.file(params[:filedata]).to_s
        request_data = Net::HTTP::Post::Multipart.new(uri.path, params)
        # ログに送信したユーザを表示するためuser_idを追加
        params[:user_id] = user_id
        http.set_debug_output($stdout)
        response_data = http.start do |h|
          LoggerFormatUtil.info_log('api_upload', params)
          h.request(request_data)
        end
      end
    end

    LoggerFormatUtil.info_log('CvsMeishiService.send', response_data.body)

    JSON.parse(response_data.body)
  rescue StandardError => e
    LoggerFormatUtil.error_log(e, response_data)
    {}
  rescue WWW::Mechanize::ResponseCodeError => e
    LoggerFormatUtil.error_log(e, response_data)
    {}
  end
end
