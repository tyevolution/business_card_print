# frozen_string_literal: true

module AwsService
  require 'aws-sdk'
  class S3
    IMAGE_PATH = (ENV['S3_DIRECTORY_NAME']).to_s
    THUMBNAIL_PATH = 'thumbnail'

    def self.upload_file(upload_path, file)
      s3_client
      begin
        @s3_client.object(upload_path).upload_file(file)
        true
      rescue Aws::S3::Errors::InvalidParameter => e
        LoggerFormatUtil.error_log(e)
        false
      end
    end

    def self.delete_file(file_path)
      return if file_path.blank?

      s3_client
      begin
        @s3_client.object(file_path).delete
        true
      rescue Aws::S3::Errors::InvalidParameter => e
        LoggerFormatUtil.error_log(e)
        false
      end
    end

    def self.get_url(image_path_array)
      return [] if image_path_array.blank?

      s3_client
      image_path_array.map { |path| URI.parse(@s3_client.object(path).presigned_url(:get, expires_in: 300)) }
    end

    def self.read_file(image_name)
      return if image_name.blank?

      require 'base64'

      image_name.slice!("#{AwsService::S3::IMAGE_PATH}/")

      s3_client
      FileUtils.mkdir_p(UploadImage::DIR_PATH) unless FileTest.exist?(UploadImage::DIR_PATH)

      @s3_client.object("#{AwsService::S3::IMAGE_PATH}/#{image_name}").download_file("#{UploadImage::DIR_PATH}/#{image_name}")

      image = File.read("#{UploadImage::DIR_PATH}/#{image_name}")
      FileUtils.rm("#{UploadImage::DIR_PATH}/#{image_name}") if File.exist?("#{UploadImage::DIR_PATH}/#{image_name}")

      "data:image/jpeg;base64,#{Base64.strict_encode64(image)}"
    end

    # AWS S3接続オブジェクトの取得
    def self.s3_client
      @s3_client ||= s3_connect
    end

    def self.s3_connect
      if AwsService::Config.configured?
        Aws::S3::Resource.new(
          region: ENV['REGION'],
          access_key_id: ENV['ACCESS_KEY_ID'],
          secret_access_key: ENV['SECRET_ACCESS_KEY']
        ).bucket((ENV['S3_BUCKET_NAME']).to_s)
      else
        Aws::S3::Resource.new(region: ENV['REGION']).bucket((ENV['S3_BUCKET_NAME']).to_s)
      end
    end
  end
end
