# frozen_string_literal: true

module AwsService
  class Config
    def self.configured?
      return false unless defined? ENV['ACCESS_KEY_ID']
      return false unless defined? ENV['SECRET_ACCESS_KEY']
      return false if ENV['ACCESS_KEY_ID'].blank?
      return false if ENV['SECRET_ACCESS_KEY'].blank?

      true
    end
  end
end
