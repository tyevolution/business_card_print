# frozen_string_literal: true

module LoggerFormatUtil
  module_function

  def error_log(e, data = '')
    Rails.logger.error("data: #{data}, error: #{e.class}, message: #{e.message}, backtrace: #{e.backtrace}")
  end

  def acriverecord_error(invalid)
    Rails.logger.error("acriverecord_error: #{invalid.record.errors}")
  end

  def info_log(method, data)
    Rails.logger.info("method: #{method}, data: #{data}")
  end
end
