// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require admin-lte/plugins/jquery/jquery.min
//= require admin-lte/plugins/jquery-ui/jquery-ui.min
//= require admin-lte/plugins/bootstrap/js/bootstrap.bundle.min
//= require admin-lte/plugins/chart.js/Chart.min
//= require admin-lte/plugins/sparklines/sparkline
//= require admin-lte/plugins/jqvmap/jquery.vmap.min
//= require admin-lte/plugins/jqvmap/maps/jquery.vmap.world
//= require admin-lte/plugins/jquery-knob/jquery.knob.min
//= require admin-lte/plugins/moment/moment.min
//= require admin-lte/plugins/daterangepicker/daterangepicker
//= require admin-lte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min
//= require admin-lte/plugins/summernote/summernote-bs4.min
//= require admin-lte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min
//= require admin-lte/plugins/ion-rangeslider/js/ion.rangeSlider.min
//= require admin-lte/plugins/fastclick/fastclick
//= require admin-lte/dist/js/adminlte.min
//= require admin-lte/plugins/morris/morris.min
//= require jquery_ujs
//= require_tree .

