function size2LSharpLeftCanvasDraw(image, size2LSCanvas, logoImage) {
  size2LSCanvas.fillStyle = '#FFFFFF';
  size2LSCanvas.fillRect(0, 0, 1061, 1568);
  size2LSCanvas.beginPath();
  size2LSCanvas.strokeStyle = 'gray';
  size2LSCanvas.line = 1;
  size2LSCanvas.strokeRect(10, 240, 1052, 637);
  size2LSCanvas.strokeRect(10, 877, 1052, 637);
  size2LSCanvas.drawImage(logoImage, 60, 88, 400, 144);
  size2LSCanvas.drawImage(image, 11, 241, 1050, 635); // 左11px 右16px
  size2LSCanvas.drawImage(image, 11, 878, 1050, 635); // 下55px
}
function size2LSharpRightCanvasDraw(image, size2LSCanvas) {
  size2LSCanvas.fillStyle = '#FFFFFF';
  size2LSCanvas.fillRect(0, 0, 1067, 1568);
  size2LSCanvas.beginPath();
  size2LSCanvas.strokeStyle = 'gray';
  size2LSCanvas.line = 1;
  size2LSCanvas.strokeRect(0, 240, 1052, 637);
  size2LSCanvas.strokeRect(0, 877, 1052, 637);
  size2LSCanvas.drawImage(image, 1, 241, 1050, 635); // 左11px 右16px
  size2LSCanvas.drawImage(image, 1, 878, 1050, 635); // 下55px
}

function size2LRicohLeftCanvasDraw(image, size2LPrintRCanvas, logoImage) {
  size2LPrintRCanvas.fillStyle = '#FFFFFF';
  size2LPrintRCanvas.fillRect(0, 0, 1059, 1550);
  size2LPrintRCanvas.beginPath();
  size2LPrintRCanvas.strokeStyle = 'gray';
  size2LPrintRCanvas.line = 1;
  size2LPrintRCanvas.strokeRect(10, 213, 1050, 636);
  size2LPrintRCanvas.strokeRect(10, 849, 1050, 636);
  size2LPrintRCanvas.drawImage(logoImage, 50, 78, 300, 108); // 上のマージン66px+12pxで計算
  size2LPrintRCanvas.drawImage(image, 11, 214, 1048, 634); // 左11px 右19px
  size2LPrintRCanvas.drawImage(image, 11, 850, 1048, 634); // 下のマージン66pxで計算
}
function size2LRicohRightCanvasDraw(image, size2LPrintRCanvas) {
  size2LPrintRCanvas.fillStyle = '#FFFFFF';
  size2LPrintRCanvas.fillRect(0, 0, 1069, 1550);
  size2LPrintRCanvas.beginPath();
  size2LPrintRCanvas.strokeStyle = 'gray';
  size2LPrintRCanvas.line = 1;
  size2LPrintRCanvas.strokeRect(0, 213, 1050, 636);
  size2LPrintRCanvas.strokeRect(0, 849, 1050, 636);
  size2LPrintRCanvas.drawImage(image, 1, 214, 1048, 634); // 左11px 右19px
  size2LPrintRCanvas.drawImage(image, 1, 850, 1048, 634); // 下のマージン66pxで計算
}

function size2LKyoceraLeftCanvasDraw(image, size2LPrintKCanvas, logoImage) {
  size2LPrintKCanvas.fillStyle = '#FFFFFF';
  size2LPrintKCanvas.fillRect(0, 0, 1069, 1548);
  size2LPrintKCanvas.beginPath();
  size2LPrintKCanvas.strokeStyle = 'gray';
  size2LPrintKCanvas.line = 1;
  size2LPrintKCanvas.strokeRect(18, 220, 1052, 637);
  size2LPrintKCanvas.strokeRect(18, 857, 1052, 637);
  size2LPrintKCanvas.drawImage(logoImage, 30, 88, 300, 108);
  size2LPrintKCanvas.drawImage(image, 19, 221, 1050, 635); // 左19px 右21px
  size2LPrintKCanvas.drawImage(image, 19, 858, 1050, 635); // 下55px
}
function size2LKyoceraRightCanvasDraw(image, size2LPrintKCanvas) {
  size2LPrintKCanvas.fillStyle = '#FFFFFF';
  size2LPrintKCanvas.fillRect(0, 0, 1069, 1548);
  size2LPrintKCanvas.beginPath();
  size2LPrintKCanvas.strokeStyle = 'gray';
  size2LPrintKCanvas.line = 1;
  size2LPrintKCanvas.strokeRect(0, 220, 1052, 637);
  size2LPrintKCanvas.strokeRect(0, 857, 1052, 637);
  size2LPrintKCanvas.drawImage(image, 1, 221, 1050, 635); // 左19px 右21px
  size2LPrintKCanvas.drawImage(image, 1, 858, 1050, 635); // 下55px
}

function sizeLRicohCanvasDraw(image, sizeLPrintRCanvas, logoImage) {
  sizeLPrintRCanvas.fillStyle = '#FFFFFF';
  sizeLPrintRCanvas.fillRect(0, 0, 1076, 1550);
  sizeLPrintRCanvas.drawImage(logoImage, 50, 78, 300, 108); // 上のマージン66px+12pxで計算
  sizeLPrintRCanvas.drawImage(image, 8, 210, 1052, 636); // 左8px 右16pxで計算
  sizeLPrintRCanvas.drawImage(image, 8, 848, 1052, 636); // 下のマージン66pxで計算
}

function sizeLRicohCanvasDrawLine(sizeLPrintRCanvas) {
  sizeLPrintRCanvas.beginPath();
  sizeLPrintRCanvas.strokeStyle = 'gray';
  sizeLPrintRCanvas.line = 1;
  sizeLPrintRCanvas.strokeRect(7, 209, 1054, 638);
  sizeLPrintRCanvas.strokeRect(7, 847, 1054, 638);
}

function sizeLKyoceraCanvasDraw(image, sizeLPrintKCanvas, logoImage) {
  sizeLPrintKCanvas.fillStyle = '#FFFFFF';
  sizeLPrintKCanvas.fillRect(0, 0, 1088, 1548);
  sizeLPrintKCanvas.drawImage(logoImage, 60, 88, 300, 108); // 上のマージン70px+18pxで計算
  sizeLPrintKCanvas.drawImage(image, 24, 206, 1051, 635); // 左24px 右13px
  sizeLPrintKCanvas.drawImage(image, 24, 843, 1051, 635); // 下のマージン70pxで計算
}

function sizeLKyoceraCanvasDrawLine(sizeLPrintKCanvas) {
  sizeLPrintKCanvas.beginPath();
  sizeLPrintKCanvas.strokeStyle = 'gray';
  sizeLPrintKCanvas.line = 1;
  sizeLPrintKCanvas.strokeRect(23, 205, 1053, 637);
  sizeLPrintKCanvas.strokeRect(23, 842, 1053, 637);
}

function sizeLSharpCanvasDraw(image, sizeLCanvas, logoImage) {
  sizeLCanvas.fillStyle = '#FFFFFF';
  sizeLCanvas.fillRect(0, 0, 1076, 1568);
  sizeLCanvas.drawImage(logoImage, 60, 88, 400, 144);
  sizeLCanvas.drawImage(image, 10, 248, 1048, 634); // 左10px 右18px
  sizeLCanvas.drawImage(image, 10, 884, 1048, 634); // 下 50px
}

function sizeLSharpCanvasDrawLine(sizeLCanvas) {
  sizeLCanvas.beginPath();
  sizeLCanvas.strokeStyle = 'gray';
  sizeLCanvas.line = 1;
  sizeLCanvas.strokeRect(9, 247, 1050, 636);
  sizeLCanvas.strokeRect(9, 883, 1050, 636);
}

function sizeLXeroxCanvasDraw(image, sizeLPrintXeroxCanvas, logoImage) {
  sizeLPrintXeroxCanvas.fillStyle = '#FFFFFF';
  sizeLPrintXeroxCanvas.fillRect(0, 0, 1076, 1568);
  sizeLPrintXeroxCanvas.drawImage(logoImage, 62, 72, 400, 144); // 上のマージン60px+12pxで計算
  sizeLPrintXeroxCanvas.drawImage(image, 14, 238, 1048, 634); // 左14px 右14px
  sizeLPrintXeroxCanvas.drawImage(image, 14, 874, 1048, 634); // 下のマージン60pxで計算
}

function sizeLXeroxCanvasDrawLine(sizeLPrintXeroxCanvas) {
  sizeLPrintXeroxCanvas.beginPath();
  sizeLPrintXeroxCanvas.strokeStyle = 'gray';
  sizeLPrintXeroxCanvas.line = 1;
  sizeLPrintXeroxCanvas.strokeRect(13, 237, 1050, 636);
  sizeLPrintXeroxCanvas.strokeRect(13, 873, 1050, 636);
}

function sizeLPrintPreviewCanvasDraw(image, sizeLPrintPreviewCanvas, logoImage) {
  sizeLPrintPreviewCanvas.fillStyle = '#FFFFFF';
  sizeLPrintPreviewCanvas.fillRect(0, 0, 514, 719);
  sizeLPrintPreviewCanvas.beginPath();
  sizeLPrintPreviewCanvas.strokeStyle = 'gray';
  sizeLPrintPreviewCanvas.line = 1;
  sizeLPrintPreviewCanvas.drawImage(logoImage, 15, 8, 200, 72);
  sizeLPrintPreviewCanvas.drawImage(image, 0, 97, 514, 311);
  sizeLPrintPreviewCanvas.drawImage(image, 0, 408, 514, 311);
  sizeLPrintPreviewCanvas.strokeRect(0, 96, 515, 311);
}

function sizeL2PrintPreviewCanvasDraw(image, size2LPrintPreviewCanvas, logoImage) {
  size2LPrintPreviewCanvas.fillStyle = '#FFFFFF';
  size2LPrintPreviewCanvas.fillRect(0, 0, 1029, 719);
  size2LPrintPreviewCanvas.beginPath();
  size2LPrintPreviewCanvas.strokeStyle = 'gray';
  size2LPrintPreviewCanvas.line = 1;
  size2LPrintPreviewCanvas.drawImage(logoImage, 30, 8, 200, 72);
  size2LPrintPreviewCanvas.drawImage(image, 0, 97, 514, 311);
  size2LPrintPreviewCanvas.drawImage(image, 0, 408, 514, 311);
  size2LPrintPreviewCanvas.drawImage(image, 515, 97, 514, 311);
  size2LPrintPreviewCanvas.drawImage(image, 515, 408, 514, 311);
  size2LPrintPreviewCanvas.strokeRect(0, 96, 515, 312);
  size2LPrintPreviewCanvas.strokeRect(514, 96, 515, 312);
  size2LPrintPreviewCanvas.strokeRect(0, 408, 515, 312);
  size2LPrintPreviewCanvas.strokeRect(514, 408, 515, 312);
}

function sizeLDecaPrintPreviewCanvasDraw(image, sizeLDecaPrintPreviewCanvas, logoImage) {
  sizeLDecaPrintPreviewCanvas.fillStyle = '#FFFFFF';
  sizeLDecaPrintPreviewCanvas.fillRect(0, 0, 719, 514);
  sizeLDecaPrintPreviewCanvas.beginPath();
  sizeLDecaPrintPreviewCanvas.strokeStyle = 'gray';
  sizeLDecaPrintPreviewCanvas.line = 1;
  sizeLDecaPrintPreviewCanvas.strokeRect(0, 79, 719, 0);
  sizeLDecaPrintPreviewCanvas.drawImage(logoImage, 15, 12, 155, 56);
  sizeLDecaPrintPreviewCanvas.drawImage(image, 0, 79, 719, 435);
}

function sizeLDecaSharpCanvasDraw(image, sizeLDecaCanvas, logoImage) {
  sizeLDecaCanvas.fillStyle = '#FFFFFF';
  sizeLDecaCanvas.fillRect(0, 0, 1568, 1076);
  sizeLDecaCanvas.drawImage(logoImage, 78, 36, 200, 72);
  sizeLDecaCanvas.drawImage(image, 0, 128, 1568, 948);
  sizeLDecaCanvas.beginPath();
  sizeLDecaCanvas.strokeStyle = 'gray';
  sizeLDecaCanvas.line = 1;
  sizeLDecaCanvas.strokeRect(0, 128, 1568, 0);
}

function sizeLDecaRicohCanvasDraw(image, sizeLDecaCanvas, logoImage) {
  sizeLDecaCanvas.fillStyle = '#FFFFFF';
  sizeLDecaCanvas.fillRect(0, 0, 1550, 1076);
  sizeLDecaCanvas.drawImage(logoImage, 78, 40, 200, 72);
  sizeLDecaCanvas.drawImage(image, 0, 139, 1550, 937);
  sizeLDecaCanvas.beginPath();
  sizeLDecaCanvas.strokeStyle = 'gray';
  sizeLDecaCanvas.line = 1;
  sizeLDecaCanvas.strokeRect(0, 139, 1550, 0);
}

function sizeLDecaKyoceraCanvasDraw(image, sizeLDecaCanvas, logoImage) {
  sizeLDecaCanvas.fillStyle = '#FFFFFF';
  sizeLDecaCanvas.fillRect(0, 0, 1548, 1088);
  sizeLDecaCanvas.drawImage(logoImage, 78, 42, 200, 72);
  sizeLDecaCanvas.drawImage(image, 0, 152, 1548, 936);
  sizeLDecaCanvas.beginPath();
  sizeLDecaCanvas.strokeStyle = 'gray';
  sizeLDecaCanvas.line = 1;
  sizeLDecaCanvas.strokeRect(0, 151, 1548, 0);
}

function sizeLDecaXeroxCanvasDraw(image, sizeLDecaCanvas, logoImage) {
  sizeLDecaCanvas.fillStyle = '#FFFFFF';
  sizeLDecaCanvas.fillRect(0, 0, 1568, 1076);
  sizeLDecaCanvas.drawImage(logoImage, 72, 32, 200, 72);
  sizeLDecaCanvas.drawImage(image, 0, 128, 1568, 948);
  sizeLDecaCanvas.beginPath();
  sizeLDecaCanvas.strokeStyle = 'gray';
  sizeLDecaCanvas.line = 1;
  sizeLDecaCanvas.strokeRect(0, 128, 1568, 0);
}

function size2LDecaPrintPreviewCanvasDraw(image, size2LDecaPrintPreviewCanvas, logoImage) {
  size2LDecaPrintPreviewCanvas.fillStyle = '#FFFFFF';
  size2LDecaPrintPreviewCanvas.fillRect(0, 0, 1029, 719);
  size2LDecaPrintPreviewCanvas.beginPath();
  size2LDecaPrintPreviewCanvas.strokeStyle = 'gray';
  size2LDecaPrintPreviewCanvas.line = 1;
  size2LDecaPrintPreviewCanvas.drawImage(logoImage, 30, 8, 200, 72);
  size2LDecaPrintPreviewCanvas.drawImage(image, 0, 97, 1029, 622);
  size2LDecaPrintPreviewCanvas.strokeRect(0, 96, 1029, 0);
}

function size2LDecaSharpCanvasDraw(image, size2LSCanvas, logoImage) {
  size2LSCanvas.fillStyle = '#FFFFFF';
  size2LSCanvas.fillRect(0, 0, 2128, 1568);
  size2LSCanvas.beginPath();
  size2LSCanvas.strokeStyle = 'gray';
  size2LSCanvas.line = 1;
  size2LSCanvas.drawImage(logoImage, 60, 88, 400, 144);
  size2LSCanvas.drawImage(image, 0, 282, 2128, 1286);
  size2LSCanvas.strokeRect(0, 281, 2128, 0);
}

function size2LDecaRicohCanvasDraw(image, size2LRCanvas, logoImage) {
  size2LRCanvas.fillStyle = '#FFFFFF';
  size2LRCanvas.fillRect(0, 0, 2128, 1550);
  size2LRCanvas.beginPath();
  size2LRCanvas.strokeStyle = 'gray';
  size2LRCanvas.line = 1;
  size2LRCanvas.drawImage(logoImage, 60, 74, 400, 144);
  size2LRCanvas.drawImage(image, 0, 264, 2128, 1286);
  size2LRCanvas.strokeRect(0, 263, 2128, 0);
}

function size2LDecaKyoceraCanvasDraw(image, size2LKCanvas, logoImage) {
  size2LKCanvas.fillStyle = '#FFFFFF';
  size2LKCanvas.fillRect(0, 0, 2138, 1548);
  size2LKCanvas.beginPath();
  size2LKCanvas.strokeStyle = 'gray';
  size2LKCanvas.line = 1;
  size2LKCanvas.drawImage(logoImage, 60, 70, 400, 144);
  size2LKCanvas.drawImage(image, 0, 256, 2138, 1292);
  size2LKCanvas.strokeRect(0, 255, 2138, 0);
}