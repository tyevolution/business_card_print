# frozen_string_literal: true

class UploadImage < ApplicationRecord
  require 'base64'
  has_many :print_api_responses, dependent: :destroy
  belongs_to :user

  SIZE_L = '07'
  SIZE_2L = '08'
  DIR_PATH = Rails.root.join('tmp', 'image')

  def save_image(user_id, trimming_file, print_file, size)
    return check_result(nil) if trimming_file.blank? || print_file.blank? || size.blank?

    FileUtils.mkdir_p(UploadImage::DIR_PATH) unless FileTest.exist?(UploadImage::DIR_PATH)

    size = size_conversion(size)
    create_print_file_name(user_id,
                           xerox: print_file[:xerox].present?,
                           sharp: print_file[:sharp].present?,
                           sharp_left: print_file[:sharp_left].present?,
                           sharp_right: print_file[:sharp_right].present?,
                           ricoh: print_file[:ricoh].present?,
                           ricoh_left: print_file[:ricoh_left].present?,
                           ricoh_right: print_file[:ricoh_right].present?,
                           kyocera: print_file[:kyocera].present?,
                           kyocera_left: print_file[:kyocera_left].present?,
                           kyocera_right: print_file[:kyocera_right].present?)

    @trimming_file_name = "#{user_id}_#{Time.current.strftime('%Y%m%d%H%M%S%L')}.jpeg"
    image_path = "#{AwsService::S3::IMAGE_PATH}/#{@trimming_file_name}"
    File.open("#{UploadImage::DIR_PATH}/#{@trimming_file_name}", 'wb') do |f|
      f.write(Base64.decode64(trimming_file['data:image/jpeg;base64,'.length..-1]))
    end
    trimming_image = AwsService::S3.upload_file(image_path, File.open("#{UploadImage::DIR_PATH}/#{@trimming_file_name}"))
    remove_trimming_file
    return check_result(nil) if trimming_image.blank?

    record = UploadImage.create!(user_id: user_id, image_path: image_path)

    save_file(print_file, size)
    response_data = api_access(size, user_id)
    remove_print_file

    PrintApiResponse.new.save_response_data(user_id, record[:id], response_data)

    check_result(response_data)
  rescue ActiveRecord::RecordInvalid => e
    LoggerFormatUtil.acriverecord_error(e)
    remove_trimming_file
    remove_print_file
    check_result(nil)
  rescue StandardError => e
    LoggerFormatUtil.error_log(e)
    remove_trimming_file
    remove_print_file
    check_result(nil)
  end

  def update_auth_code(user_id, print_file, size, update_id)
    return check_result(nil) if print_file.blank? || size.blank?

    FileUtils.mkdir_p(UploadImage::DIR_PATH) unless FileTest.exist?(UploadImage::DIR_PATH)

    size = size_conversion(size)
    create_print_file_name(user_id,
                           xerox: print_file[:xerox].present?,
                           sharp: print_file[:sharp].present?,
                           sharp_left: print_file[:sharp_left].present?,
                           sharp_right: print_file[:sharp_right].present?,
                           ricoh: print_file[:ricoh].present?,
                           ricoh_left: print_file[:ricoh_left].present?,
                           ricoh_right: print_file[:ricoh_right].present?,
                           kyocera: print_file[:kyocera].present?,
                           kyocera_left: print_file[:kyocera_left].present?,
                           kyocera_right: print_file[:kyocera_right].present?)

    record = UploadImage.find(update_id)
    return check_result(nil) unless record.user_id == user_id

    save_file(print_file, size)
    response_data = api_access(size, user_id)
    remove_print_file

    PrintApiResponse.new.save_response_data(user_id, record[:id], response_data)

    check_result(response_data)
  rescue ActiveRecord::RecordInvalid => e
    LoggerFormatUtil.acriverecord_error(e)
    remove_print_file
    check_result(nil)
  rescue StandardError => e
    LoggerFormatUtil.error_log(e)
    remove_print_file
    check_result(nil)
  end

  def api_access(size, user_id)
    response_data = {}

    response_data[:xerox] = if size == UploadImage::SIZE_L
                              CvsMeishiService.new.send(@print_file_name[:xerox], size, 1, user_id)
                            else
                              {}
                            end
    response_data[:sharp] = CvsMeishiService.new.send(@print_file_name[:sharp], size, 2, user_id)
    response_data[:ricoh] = CvsMeishiService.new.send(@print_file_name[:ricoh], size, 3, user_id)
    response_data[:kyocera] = CvsMeishiService.new.send(@print_file_name[:kyocera], size, 4, user_id)

    response_data
  end

  def delete_image(id)
    data = UploadImage.find(id)
    return false unless AwsService::S3.delete_file(data[:image_path])

    data.destroy.present?
  end

  private

  def check_result(data)
    non_content = { contents_code: '', expiration_date: '' }

    if data.present?
      data.each do |key, value|
        data[key] = non_content if value.blank?
      end
      data
    else
      { xerox: non_content, sharp: non_content, ricoh: non_content, kyocera: non_content }
    end
  end

  def remove_trimming_file
    FileUtils.rm("#{UploadImage::DIR_PATH}/#{@trimming_file_name}") if File.exist?("#{UploadImage::DIR_PATH}/#{@trimming_file_name}")
  end

  def remove_print_file
    @print_file_name.each do |_k, v|
      FileUtils.rm("#{UploadImage::DIR_PATH}/#{v}") if File.exist?("#{UploadImage::DIR_PATH}/#{v}")
    end
  end

  def size_conversion(size)
    case size.to_i
    when 1, 3
      UploadImage::SIZE_L
    when 2, 4
      UploadImage::SIZE_2L
    else
      ''
    end
  end

  def create_print_file_name(user_id, existence_flag)
    now = Time.current.strftime('%Y%m%d%H%M%S%L')
    @print_file_name = {}

    @print_file_name[:xerox] = "xerox_print_image_#{user_id}_#{now}.jpeg" if existence_flag[:xerox]
    @print_file_name[:sharp_left] = "sharp_left_print_image_#{user_id}_#{now}.jpeg" if existence_flag[:sharp_left]
    @print_file_name[:sharp_right] = "sharp_right_print_image_#{user_id}_#{now}.jpeg" if existence_flag[:sharp_right]
    @print_file_name[:ricoh_left] = "ricoh_left_print_image_#{user_id}_#{now}.jpeg" if existence_flag[:ricoh_left]
    @print_file_name[:ricoh_right] = "ricoh_right_print_image_#{user_id}_#{now}.jpeg" if existence_flag[:ricoh_right]
    @print_file_name[:kyocera_left] = "kyocera_left_print_image_#{user_id}_#{now}.jpeg" if existence_flag[:kyocera_left]
    @print_file_name[:kyocera_right] = "kyocera_right_print_image_#{user_id}_#{now}.jpeg" if existence_flag[:kyocera_right]
    @print_file_name[:sharp] = "sharp_print_image_#{user_id}_#{now}.jpeg" if existence_flag[:sharp]
    @print_file_name[:ricoh] = "ricoh_print_image_#{user_id}_#{now}.jpeg" if existence_flag[:ricoh]
    @print_file_name[:kyocera] = "kyocera_print_image_#{user_id}_#{now}.jpeg" if existence_flag[:kyocera]
  end

  def save_file(print_file, size)
    @print_file_name.each do |k, v|
      next if (k == :xerox) && (size != UploadImage::SIZE_L)

      File.open("#{UploadImage::DIR_PATH}/#{v}", 'wb') do |f|
        f.write(Base64.decode64(print_file[k]['data:image/jpeg;base64,'.length..-1]))
      end
    end

    join_file if size == UploadImage::SIZE_2L && join_judge
  end

  def join_judge
    return true if @print_file_name[:sharp_left].present? && @print_file_name[:sharp_right].present?
    return true if @print_file_name[:ricoh_left].present? && @print_file_name[:ricoh_right].present?
    return true if @print_file_name[:kyocera_left].present? && @print_file_name[:kyocera_right].present?

    false
  end

  def join_file
    require 'rmagick'

    type_array = %i[sharp ricoh kyocera]

    type_array.each do |type|
      image = Magick::ImageList.new("#{UploadImage::DIR_PATH}/#{@print_file_name[:"#{type}_left"]}",
                                    "#{UploadImage::DIR_PATH}/#{@print_file_name[:"#{type}_right"]}").append(false) # falseだと左右につなげる。trueだと上下につなげる
      @print_file_name[type] = @print_file_name[:"#{type}_left"].sub('_left', '')
      image.write("#{UploadImage::DIR_PATH}/#{@print_file_name[type]}")
    end
  end
end
