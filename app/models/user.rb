# frozen_string_literal: true

class User < ApplicationRecord
  has_many :upload_images, dependent: :destroy
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :confirmable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def password_required?
    super if confirmed?
  end

  def active_for_authentication?
    super && !deleted_at
  end

  def soft_delete
    now_time = Time.current
    deleted_email = "#{self.email}_deleted_#{I18n.l(now_time, format: :deleted_at)}"
    self.assign_attributes(email: deleted_email, deleted_at: now_time)
    self.skip_reconfirmation!
    self.save!
  end
end
