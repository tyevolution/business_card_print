# frozen_string_literal: true

class Inquiry < ApplicationRecord
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i.freeze
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }
  validates :content, presence: true, length: { maximum: 400, message: 'お問い合わせ内容は400文字以内で入力してください。' }

  BUG = 2
  BUG_STRING = '不具合'
  REQUEST = 3
  REQUEST_STRING = 'ご要望'
  OTHER = 1
  OTHER_STRING = 'その他'
end
