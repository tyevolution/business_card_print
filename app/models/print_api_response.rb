# frozen_string_literal: true

class PrintApiResponse < ApplicationRecord
  belongs_to :upload_image

  def save_response_data(user_id, record_id, response_data)
    return if response_data.blank?

    response_data.each do |key, value|
      record = PrintApiResponse.new
      record.upload_image_id = record_id
      record.user_id = user_id
      record.result = value['result']
      record.contents_code = value['contents_code']
      record.page_count = value['page_count']
      record.expiration_date = value['expiration_date'].to_s.in_time_zone
      record.error_message = value['error_message']
      record.cvtype = case key.to_s
                      when 'xerox'
                        1
                      when 'sharp'
                        2
                      when 'ricoh'
                        3
                      when 'kyocera'
                        4
                      else
                        0
                      end
      record.save!
    end
  end
end
