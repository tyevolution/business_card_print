# frozen_string_literal: true

class RegistrationCompleteController < ApplicationController
  def index; end

  private

  def use_before_action?
    false
  end
end
