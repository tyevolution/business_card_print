# frozen_string_literal: true

class ImgHistoryController < ApplicationController
  before_action :authenticate_user!
  def index
    record = UploadImage.where(user_id: current_user[:id])
    @image_info = []
    AwsService::S3.get_url(record.pluck(:image_path)).each_with_index do |v, i|
      @image_info.push(url: v, id: record[i][:id])
    end
  end
end
