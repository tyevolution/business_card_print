# frozen_string_literal: true

class LpController < ApplicationController
  def index; end

  private

  def use_before_action?
    false
  end
end
