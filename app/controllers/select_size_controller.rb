# frozen_string_literal: true

class SelectSizeController < ApplicationController
  before_action :authenticate_user!
  def index
    @picture_id = params[:id].presence || ''
  end
end
