# frozen_string_literal: true

class PrivacyController < ApplicationController
  def index; end

  private

  def use_before_action?
    false
  end
end
