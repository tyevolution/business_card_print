# frozen_string_literal: true

module Api
  module V1
    class CodeAuthController < ActionController::API
      before_action :authenticate_user!

      def create
        data = params.permit(
          :trimming_file,
          :print_file_xerox,
          :print_file_sharp,
          :print_file_sharp_left,
          :print_file_sharp_right,
          :print_file_ricoh,
          :print_file_ricoh_left,
          :print_file_ricoh_right,
          :print_file_kyocera,
          :print_file_kyocera_left,
          :print_file_kyocera_right,
          :size
        )
        result = UploadImage.new.save_image(
          current_user[:id],
          data[:trimming_file],
          {
            xerox: data[:print_file_xerox],
            sharp: data[:print_file_sharp],
            ricoh: data[:print_file_ricoh],
            kyocera: data[:print_file_kyocera],
            sharp_left: data[:print_file_sharp_left],
            sharp_right: data[:print_file_sharp_right],
            ricoh_left: data[:print_file_ricoh_left],
            ricoh_right: data[:print_file_ricoh_right],
            kyocera_left: data[:print_file_kyocera_left],
            kyocera_right: data[:print_file_kyocera_right]
          },
          data[:size]
        )

        render json: { result: result }
      end

      def update
        data = params.permit(
          :trimming_file,
          :print_file_xerox,
          :print_file_sharp,
          :print_file_sharp_left,
          :print_file_sharp_right,
          :print_file_ricoh,
          :print_file_ricoh_left,
          :print_file_ricoh_right,
          :print_file_kyocera,
          :print_file_kyocera_left,
          :print_file_kyocera_right,
          :size,
          :picture_id
        )
        result = UploadImage.new.update_auth_code(
          current_user[:id],
          {
            xerox: data[:print_file_xerox],
            sharp: data[:print_file_sharp],
            ricoh: data[:print_file_ricoh],
            kyocera: data[:print_file_kyocera],
            sharp_left: data[:print_file_sharp_left],
            sharp_right: data[:print_file_sharp_right],
            ricoh_left: data[:print_file_ricoh_left],
            ricoh_right: data[:print_file_ricoh_right],
            kyocera_left: data[:print_file_kyocera_left],
            kyocera_right: data[:print_file_kyocera_right]
          },
          data[:size],
          data[:picture_id]
        )

        render json: { result: result }
      end
    end
  end
end
