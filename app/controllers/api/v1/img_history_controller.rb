# frozen_string_literal: true

module Api
  module V1
    class ImgHistoryController < ActionController::API
      before_action :authenticate_user!

      def read
        render json: { image: AwsService::S3.read_file(UploadImage.find(params[:id])[:image_path]) }
      end

      def delete
        render json: { status: UploadImage.new.delete_image(params[:id]) }
      end
    end
  end
end
