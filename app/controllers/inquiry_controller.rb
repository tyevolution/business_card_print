# frozen_string_literal: true

class InquiryController < ApplicationController
  def new; end

  def create
    data = parse_params(params)
    create_result = Inquiry.create(data)
    mail_result = NotificationMailer.send_inquiry(data).deliver_now

    render json: { result: create_result.present? && mail_result.present? }
  end

  private

  def use_before_action?
    false
  end

  def parse_params(params)
    params.permit(
      :inquiry_type,
      :email,
      :content
    )
  end
end
