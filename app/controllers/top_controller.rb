# frozen_string_literal: true

class TopController < ApplicationController
  def index
    @maintenance_content = SystemNotification.where(display_flag: true)
  end

  private

  def use_before_action?
    false
  end
end
