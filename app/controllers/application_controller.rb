# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :store_current_location, if: :use_before_action?

  def after_sign_in_path_for(_resource)
    stored_location_for(_resource) || top_path
  end

  def after_sign_out_path_for(_resource)
    top_path
  end

  # オーバーライドしてfalseにしたコントローラーではbefore_actionを実行しない
  def use_before_action?
    # devise_controllerでは実行しない
    return false if devise_controller?

    true
  end

  private

  def store_current_location
    store_location_for(:user, request.url)
  end
end
