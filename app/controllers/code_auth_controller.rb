# frozen_string_literal: true

class CodeAuthController < ApplicationController
  before_action :authenticate_user!
  def index
    @xerox_code = { code: params[:result][:xerox][:contents_code], expiration_date: params[:result][:xerox][:expiration_date] }
    @sharp_code = { code: params[:result][:sharp][:contents_code], expiration_date: params[:result][:sharp][:expiration_date] }
    @ricoh_code = { code: params[:result][:ricoh][:contents_code], expiration_date: params[:result][:ricoh][:expiration_date] }
    @kyocera_code = { code: params[:result][:kyocera][:contents_code], expiration_date: params[:result][:kyocera][:expiration_date] }
    NotificationMailer.send_auth_code(current_user, xerox: @xerox_code, sharp: @sharp_code, ricoh: @ricoh_code, kyocera: @kyocera_code).deliver_now
  end

  def create
    data = params.permit(:result_code)
    redirect_to action: 'index', result: JSON.parse(data[:result_code])
  end
end
