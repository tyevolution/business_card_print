# frozen_string_literal: true

class ImagesUpPageController < ApplicationController
  before_action :authenticate_user!

  def index
    @app_flag = params[:app] == 'app'
  end
end
