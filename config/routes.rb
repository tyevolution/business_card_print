# frozen_string_literal: true

Rails.application.routes.draw do
  root to: 'lp#index'
  get 'top', to: 'top#index'
  get 'images_up_page', to: 'images_up_page#index'
  get 'images_up_page/:app', to: 'images_up_page#index', as: 'app_image_up_page'
  get 'reset_mail', to: 'reset_mail#index'
  get 'registration_mail', to: 'registration_mail#index'
  get 'registration_complete', to: 'registration_complete#index'
  get 'privacy', to: 'privacy#index'

  namespace :img_history do
    get '/', action: :index
  end

  namespace :select_size do
    get '/:id', action: :index, as: 'update'
    get '/', action: :index
  end

  namespace :code_auth do
    post '/', action: :create
    get '/', action: :index
  end

  namespace :inquiry do
    get '/', action: :new
    post '/', action: :create, as: 'send'
  end

  namespace :api do
    namespace :v1 do
      namespace :code_auth do
        post '/', action: :create
        put '/', action: :update
      end
      namespace :img_history do
        get '/', action: :read
        delete '/', action: :delete
      end
    end
  end

  devise_for :users, controllers: {
    registrations: 'users/registrations',
    confirmations: 'users/confirmations',
    passwords: 'users/passwords'
  }
  devise_scope :user do
    patch 'users/confirmation', to: 'users/confirmations#confirm', as: 'patch_confirmation'
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  mount LetterOpenerWeb::Engine, at: '/letter_opener' if Rails.env.development?
end
