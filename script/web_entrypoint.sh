#!/bin/sh

date +%Y%m%d%H%I%s
if [ "${RAILS_ENV}" = "production" ]; then
  rake assets:precompile
fi

rm tmp/pids/server.pid

rails s -e $RAILS_ENV