#####################################
# Terraform State Settings
#####################################

terraform {
  backend "s3" {
    bucket = "mymeishi-business-card"
    key    = "terraform.tfstate.aws"
    region = "ap-northeast-1"
  }
}
