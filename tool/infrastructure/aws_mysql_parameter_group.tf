resource "aws_db_parameter_group" "default" {
    name = "rds-pg"
    family = "mysql5.6"
    description = "Managed by Terraform"

    parameter {
      name = "time_zone"
      value = "Asia/Tokyo"
    }
}