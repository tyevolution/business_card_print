resource "aws_db_instance" "db" {
    identifier              = "dbinstance"
    allocated_storage       = 5
    engine                  = "mysql"
    engine_version          = "5.6.40"
    instance_class          = "${var.db_instance_type}"
    storage_type            = "gp2"
    username                = "${var.db_username}"
    password                = "${var.db_password}"
    publicly_accessible     = true
    backup_retention_period = 1
    vpc_security_group_ids  = ["${aws_security_group.db_security_group.id}"]
    db_subnet_group_name    = "${aws_db_subnet_group.vpc_main_db_subnet_group.name}"
}