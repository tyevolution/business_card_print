#####################################
# General Settings
#####################################

variable "access_key" {}
variable "secret_key" {}
variable "account_id" {}

variable "region" {
  default = "ap-northeast-1"
}

variable "ses_region" {
  default = "us-east-1"
}

variable "az1" {
  default = "ap-northeast-1a"
}

variable "az2" {
  default = "ap-northeast-1c"
}

variable "ec2_key_name" {}

#####################################
# VPC Settings
#####################################

variable "vpc_main_cidr" {
  default = "172.16.0.0/16"
}

variable "vpc_main_cidr_public1" {
  default = "172.16.1.0/24"
}

variable "vpc_main_cidr_public2" {
  default = "172.16.2.0/24"
}

variable "vpc_main_cidr_private1" {
  default = "172.16.21.0/24"
}

variable "vpc_main_cidr_private2" {
  default = "172.16.22.0/24"
}

variable "vpc_main_cidr_collector1" {
  default = "172.16.21.0/24"
}

variable "vpc_main_cidr_collector2" {
  default = "172.16.22.0/24"
}

variable "vpc_main_cidr_db1" {
  default = "172.16.11.0/24"
}

variable "vpc_main_cidr_db2" {
  default = "172.16.12.0/24"
}

#####################################
# Database Settings
#####################################

variable "db_username" {}
variable "db_password" {}

variable "db_instance_type" {
  default = "db.t2.micro"
}

#####################################
# Application Settings
#####################################

variable "solution_stack_name" {
  default = "64bit Amazon Linux 2018.03 v2.12.17 running Docker 18.06.1-ce"
}

variable "rails_env" {
  default = "production"
}

variable "app_name" {}

variable "web_healthcheck_path" {
  default = "/robots.txt"
}

variable "secret_key_base" {}

variable "web_instance_type" {
  default = "t2.micro"
}

variable "loadbalancer_certificate_arn" {}
variable "develop_domain" {}
variable "production_domain" {}
variable "s3_bucket_name" {}
variable "develop_api_url" {}
variable "production_api_url" {}
variable "develop_inquiry_mail_address" {}
variable "production_inquiry_mail_address" {}

#####################################
# Network Settings
#####################################


variable "ip_whitelist_for_sg" {
  default = [
    "60.67.58.60/32"
  ]
}

#####################################
# ElasticBeanstalk Application Settings
#####################################
variable "eb_environment_variables" {
  type = "list"

  default = [
    {
      namespace = "aws:elasticbeanstalk:application:environment"
      name      = "TZ"
      value     = "Asia/Tokyo"
    },
    {
      namespace = "aws:elasticbeanstalk:application:environment"
      name      = "DB_PORT"
      value     = "3306"
    },
    {
      namespace = "aws:elasticbeanstalk:application:environment"
      name      = "RAILS_LOG_TO_STDOUT"
      value     = "1"
    },
  ]
}
