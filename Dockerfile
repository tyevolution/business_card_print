FROM ruby:2.5.3

RUN curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
  apt-get -qq update && \
  apt-get -qq -y install --no-install-recommends \
  nodejs graphviz build-essential libpq-dev postgresql-client apt-transport-https libopencv-dev && \
  apt-get clean && rm -rf /var/cache/apt/ && rm -rf /var/lib/apt/lists/*

RUN npm install -g yarn

ENV APP_ROOT /opt/application/current
RUN mkdir -p $APP_ROOT
WORKDIR $APP_ROOT

### Install packages and gems
ADD Gemfile ${APP_ROOT}/Gemfile
ADD Gemfile.lock ${APP_ROOT}/Gemfile.lock
RUN bundle install

ADD package.json ${APP_ROOT}/package.json
ADD yarn.lock ${APP_ROOT}/yarn.lock
RUN yarn install --pure-lockfile

COPY . /opt/application/current

ENTRYPOINT ["sh", "./script/web_entrypoint.sh"]