class CreateSystemNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :system_notifications do |t|
      t.text :content
      t.boolean :display_flag, null: false, default: false
      t.datetime :start_time
      t.datetime :end_time

      t.timestamps
    end
  end
end
