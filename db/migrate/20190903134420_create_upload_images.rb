class CreateUploadImages < ActiveRecord::Migration[5.2]
  def change
    create_table :upload_images do |t|
      t.belongs_to :user, null:false
      t.text :image_path
      t.text :thumbnail_path
      t.integer :result
      t.string :contents_code
      t.integer :page_count
      t.datetime :expiration_date
      t.string :error_message

      t.timestamps
    end
  end
end
