class CreateInquiries < ActiveRecord::Migration[5.2]
  def change
    create_table :inquiries do |t|
      t.text :email, null: false
      t.integer :inquiry_type
      t.text :content, null: false

      t.timestamps
    end
  end
end
