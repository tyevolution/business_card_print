class CreatePrintApiResponses < ActiveRecord::Migration[5.2]
  def change
    create_table :print_api_responses do |t|
      t.belongs_to :upload_image, null:false
      t.integer :user_id
      t.integer :result
      t.string :contents_code
      t.integer :page_count
      t.datetime :expiration_date
      t.string :error_message
      t.integer :cvtype

      t.timestamps
    end
  end
end
