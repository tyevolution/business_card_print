class RemoveColumnToUploadImage < ActiveRecord::Migration[5.2]
  def change
    remove_column :upload_images, :result
    remove_column :upload_images, :contents_code
    remove_column :upload_images, :page_count
    remove_column :upload_images, :expiration_date
    remove_column :upload_images, :error_message
  end
end
