# frozen_string_literal: true

FactoryBot.define do
  factory :upload_image_success_1, class: UploadImage do
    id { 1 }
    user_id { 1 }
    image_path { '' }
    thumbnail_path { '' }
    result { 1 }
    contents_code { 'CVM12345' }
    page_count { 1 }
    expiration_date { '20190901000000'.in_time_zone }
    error_message { '' }
  end
end
