# frozen_string_literal: true

FactoryBot.define do
  factory :user_1, class: User do
    id { 1 }
    email { 'test@factory.com' }
    password { '12345678' }
    confirmed_at { Time.current }
  end
end
