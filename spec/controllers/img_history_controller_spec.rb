# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ImgHistoryController, type: :controller do
  describe 'GET #index' do
    it 'returns http success' do
      User.destroy_all
      user = FactoryBot.create(:user_1)
      sign_in user
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET #read' do
    it 'returns http success' do
      User.destroy_all
      user = FactoryBot.create(:user_1)
      sign_in user

      get :read, params: {
        image_name: '1_20190924011653940.jpeg'
      }

      result = JSON.parse(response.body)

      p result

      expect(response).to have_http_status(:success)
    end
  end
end
