# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UploadImage, type: :model do
  describe 'save_image' do
    before do
      UploadImage.destroy_all
      User.destroy_all
      # @trimming_file = fixture_file_upload('mikudroid.jpg', 'image/jpeg', true)
      @print_file = { sharp: 'data:image/jpeg;base64, asrt4wa', ricoh: 'data:image/jpeg;base64, asrt4wa', kyocera: 'data:image/jpeg;base64, asrt4wa' }
      # @print_file = fixture_file_upload('mikudroid.jpg', 'image/jpeg', true)
      @trimming_file = 'data:image/jpeg;base64, asrt4wa'
    end

    it 'api_success' do
      user = FactoryBot.create(:user_1)
      response = { result: 1, contents_code: 'CVM12345', page_count: 1, expiration_date: '20190901000000' }
      obj = CvsMeishiService.new
      allow(CvsMeishiService).to receive(:new).and_return(obj)
      allow(obj).to receive(:send).and_return(response)
      allow(AwsService::S3).to receive(:upload_file).and_return(true)

      user_id = user[:id]
      size = 1
      result = UploadImage.new.save_image(user_id, @trimming_file, @print_file, size)
      expect(result).to eq(response)

      record = UploadImage.find_by(user_id: user_id)
      expect(record[:result]).to eq(1)
      expect(record[:contents_code]).to eq('CVM12345')
      expect(record[:page_count]).to eq(1)
      expect(record[:expiration_date]).to eq('Sun, 01 Sep 2019 00:00:00 JST +09:00')
      expect(record[:error_message]).to eq(nil)
    end

    it 'api_fail' do
      user = FactoryBot.create(:user_1)
      response = { result: 0, error_message: 'ファイルがありません' }
      obj = CvsMeishiService.new
      allow(CvsMeishiService).to receive(:new).and_return(obj)
      allow(obj).to receive(:send).and_return(response)
      allow(AwsService::S3).to receive(:upload_file).and_return(true)

      user_id = user[:id]
      size = 1
      result = UploadImage.new.save_image(user_id, @trimming_file, @print_file, size)
      expect(result).to eq(response)

      record = UploadImage.find_by(user_id: user_id)
      expect(record[:result]).to eq(0)
      expect(record[:contents_code]).to eq(nil)
      expect(record[:page_count]).to eq(nil)
      expect(record[:expiration_date]).to eq(nil)
      expect(record[:error_message]).to eq('ファイルがありません')
    end
  end

  describe 'update_auth_code' do
    before do
      UploadImage.destroy_all
      User.destroy_all
      # @trimming_file = fixture_file_upload('mikudroid.jpg', 'image/jpeg', true)
      @trimming_file = 'data:image/jpeg;base64, asrt4wa'
      # @print_file = fixture_file_upload('mikudroid.jpg', 'image/jpeg', true)
      @print_file = 'data:image/jpeg;base64, asrt4wa'
    end

    it 'api_success' do
      FactoryBot.create(:user_1)
      update_target = FactoryBot.create(:upload_image_success_1)

      response = { result: 1, contents_code: 'CVM12345', page_count: 1, expiration_date: '20190901000000' }
      obj = CvsMeishiService.new
      allow(CvsMeishiService).to receive(:new).and_return(obj)
      allow(obj).to receive(:send).and_return(response)
      allow(AwsService::S3).to receive(:upload_file).and_return(true)

      user_id = update_target[:user_id]
      size = 1
      result = UploadImage.new.update_auth_code(user_id, @print_file, size, update_target[:id])
      expect(result).to eq(response)

      record = UploadImage.where(user_id: user_id)
      expect(record.count).to eq(1)
      expect(record[0][:result]).to eq(1)
      expect(record[0][:contents_code]).to eq('CVM12345')
      expect(record[0][:page_count]).to eq(1)
      expect(record[0][:expiration_date]).to eq('Sun, 01 Sep 2019 00:00:00 JST +09:00')
      expect(record[0][:error_message]).to eq(nil)
    end

    it 'api_fail' do
      FactoryBot.create(:user_1)
      update_target = FactoryBot.create(:upload_image_success_1)

      response = { result: 0, error_message: 'ファイルがありません' }
      obj = CvsMeishiService.new
      allow(CvsMeishiService).to receive(:new).and_return(obj)
      allow(obj).to receive(:send).and_return(response)
      allow(AwsService::S3).to receive(:upload_file).and_return(true)

      user_id = update_target[:user_id]
      size = 1
      result = UploadImage.new.update_auth_code(user_id, @print_file, size, update_target[:id])
      expect(result).to eq(response)

      record = UploadImage.where(user_id: user_id)
      expect(record.count).to eq(1)
      expect(record[0][:result]).to eq(0)
      expect(record[0][:contents_code]).to eq(nil)
      expect(record[0][:page_count]).to eq(nil)
      expect(record[0][:expiration_date]).to eq(nil)
      expect(record[0][:error_message]).to eq('ファイルがありません')
    end
  end
end
