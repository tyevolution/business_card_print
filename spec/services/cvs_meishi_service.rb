# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CvsMeishiService, type: :service do
  describe 'api test' do
    before do
      @print_file = fixture_file_upload('mikudroid.jpg', 'image/jpeg', true)
      WebMock.allow_net_connect!(net_http_connect_on_start: true)
      WebMock.stub_request(:post, ENV['API_URL'].to_s).to_return(
        body: { result: 1, contents_code: 'CVM12345', page_count: 1, expiration_date: '20190901000000' }.to_json,
        status: 200,
        headers: { 'Content-Type' => 'application/json' }
      )
    end

    it 'api success' do
      size = UploadImage::SIZE_L
      response = { result: 1, contents_code: 'CVM12345', page_count: 1, expiration_date: '20190901000000' }
      result = CvsMeishiService.new.send(@print_file, size)
      expect(result['result']).to eq(response[:result])
      expect(result['contents_code']).to eq(response[:contents_code])
      expect(result['page_count']).to eq(response[:page_count])
      expect(result['expiration_date']).to eq(response[:expiration_date])
    end
  end
end
